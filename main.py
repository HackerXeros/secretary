# Main Core
# Module Import
import speech_recognition as sr
import win32com.client as wincl
import json
import random
import os
import pyttsx
from konlpy.tag import Kkma
from collections import OrderedDict
from pprint import pprint
from datetime import datetime

# Speech_Recognition Abbreviation Setting
r = sr.Recognizer()

# User Microphone Setting
user_mic = sr.Microphone()

# KoNLPy Setting
kkma = Kkma()

# Json Parsing
with open("setting.json", encoding="utf-8") as setting_file:
    setting_data = json.load(setting_file, object_pairs_hook=OrderedDict)

# Default Info Setting
# User Name
username = setting_data["username"]
botname = setting_data["botname"]

# Answer Lists
# Bot
class botanswer:
    # Positive answer
    positive_answer = ["네 알겠습니다"
    "말씀하신대로 진행하겠습니다"
    ]

    # Negative answer
    negative_answer = ["아니요"

    ]

    # Greeting
    greeting_answer = ["네 안녕하세요",
    "저도 반가워요."
    ]

    # Calling
    call = [username]

    # Feeling
    feel = ["저는 기분이 좋아요"]

# User
class useranswer:
    # Positive answer
    positive_answer = ["어",
    "응",
    "그래",
    "네",
    "맞아"
    ]

    # Negative answer
    negative_answer = ["아니",
    "싫어"

    ]

    # Greeting
    greeting_answer = ["안녕하세요",
    "안녕",
    "헬로"
    ]
    
    # Calling
    call = [botname]

# Serching Core
def search(keyword, engine):
    if engine == "youtube":
        os.system("start https://www.youtube.com/results?search_query={}".format(keyword))
    elif engine == "google":
        os.system("start https://www.google.com/search?source=hp&ei=rJclXIayNoGm8AWC47C4CA&q={}".format(keyword))

# Reporting Core
def report(usersay, botsay):
    now = datetime.now()
    filename = "log/{}-{}-{}.log".format(now.year, now.month, now.day)
    ctime = "{}:{}:{}".format(now.hour, now.minute, now.second)
    usersay = "{} | User:{}".format(ctime, usersay)
    botsay = "{} | Bot:{}".format(ctime, botsay)
    f = open(filename, mode = "a+t", encoding = "utf-8")
    f.writelines("\n".join([usersay, botsay, "\n"]))
    f.close

# Speaking Core
def speak_core(bot_speaking):
    if os.name == "nt":
        speak = wincl.Dispatch("SAPI.SpVoice")
        speak.Speak(bot_speaking)
    elif os.name == "posix":
        engine = pyttsx
        engine.say(bot_speaking)

# Main Core
def main():
    with user_mic as source:
        r.adjust_for_ambient_noise(source)
        audio = r.listen(source)
    try:
        bot_say = None
        user_say = r.recognize_google(audio, language = "ko-KR")
        # 아래 코드는 문장을 한국어 형태소 분류방식에 따라 분류해줌
        #user_say_shape = kkma.pos(user_say)

        # greeting
        if user_say in useranswer.greeting_answer:
            bot_say = random.choice(botanswer.greeting_answer)
            speak_core(bot_say)
            report(user_say, bot_say)
        report(user_say, bot_say)
        print("User : ", user_say)
        print("Max : ", bot_say)
            
    except:
        pass

while True:
    main()