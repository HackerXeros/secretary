# Secretary
  
일정관리(스케줄링), 업무 자동화 처리 등 생활의 편리함을 위해 개발된 인공지능 비서입니다  
  
# Release  
  
Version : 1.4.1  
First Update : 2018-12-24  
Last Update : 2018-12-28  
  
# Update Log  
  
(대규모 패치).(기능 추가).(버그 수정)  
2018-12-24 : 개발 시작 1.0.0  
2018-12-25 : 웹사이트 검색 기능 추가 1.1.0  
2018-12-25 : 한국어 형태소 분석기능 추가 1.2.0  
2018-12-28 : setting.json 파일에서 사용자, 봇 이름 변경 기능 추가  1.3.0  
2018-12-28 : log 디렉토리에 현재날짜.log파일을 생성하고 대화내역을 로깅하는 기능 추가 1.4.0
2018-12-28 : 봇의 답변 영역에 없는 문자열을 유저가 말 할 경우 로깅이 안되는 버그 해결(봇의 채팅은 None으로 기록됨) 1.4.1
  
# Bug  

# Update Expected  
  
스케줄링 기능 그리드 알고리즘을 이용하여 개발  
  
# Links  
  
개발에 도움이 되었던 링크들  

https://konlpy-ko.readthedocs.io/ko/v0.4.3/ : 한국어 형태소 처리  
http://exagen.tistory.com/m/notice/63?fbclid=IwAR0StMi3-upXzkxRoPqkHNw73r6VNWCeMWT1I6mnXK_UNAbLNKKFqUV0B_E : 한국어 챗봇  